package com.whatsappcenter.covertidor;

import com.whatsappcenter.dto.RolDTO;
import com.whatsappcenter.modelo.Rol;

public class RolConverter implements AbstractConverter<Rol, RolDTO> {


    @Override
    public Rol fromDto(RolDTO dto) {
        Rol entity = new Rol();
        entity.setIdrol(dto.getIdrol());
        entity.setDescripcion(dto.getDescripcion());
        return entity;
    }

    @Override
    public RolDTO fromEntity(Rol entity) {
        RolDTO dto = new RolDTO();
        dto.setIdrol(entity.getIdrol());
        dto.setDescripcion(entity.getDescripcion());
        return dto;
    }
}


