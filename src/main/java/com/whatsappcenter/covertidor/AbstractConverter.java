package com.whatsappcenter.covertidor;

import java.util.List;
import java.util.stream.Collectors;

public interface AbstractConverter<E,D> {

    public abstract E fromDto(D dto);

    public abstract D fromEntity(E entity);

    public default List<E> fromDto(List<D> dtos){
        return dtos.stream().map(dto -> fromDto(dto)).collect(Collectors.toList());
    }

    public default List<D> fromEntity(List<E> entities){
        return entities.stream().map(entity -> fromEntity(entity)).collect(Collectors.toList());
    }

}
