package com.whatsappcenter.covertidor;

import com.whatsappcenter.dto.NumeroTelefonoDTO;
import com.whatsappcenter.modelo.Empresa;
import com.whatsappcenter.modelo.NumeroTelefono;

public class NumeroTelefonoConverter implements AbstractConverter<NumeroTelefono, NumeroTelefonoDTO> {


    @Override
    public NumeroTelefono fromDto(NumeroTelefonoDTO dto) {
        NumeroTelefono entity = new NumeroTelefono();
        entity.setIdnumero(dto.getIdnumero());
        entity.setIdempresa(new Empresa(dto.getIdempresa()));
        entity.setNumero(dto.getNumero());
        entity.setInstance(dto.getInstance());
        entity.setToken(dto.getToken());
        return entity;
    }

    @Override
    public NumeroTelefonoDTO fromEntity(NumeroTelefono entity) {
        NumeroTelefonoDTO dto = new NumeroTelefonoDTO();
        dto.setIdnumero(entity.getIdnumero());
        dto.setIdempresa(entity.getIdempresa().getIdempresa());
        dto.setRazonsocial(entity.getIdempresa().getRazonsocial());
        dto.setNumero(entity.getNumero());
        dto.setInstance(entity.getInstance());
        dto.setToken(entity.getToken());
        return dto;
    }
}


