package com.whatsappcenter.covertidor;

import com.whatsappcenter.dto.UsuarioDTO;
import com.whatsappcenter.modelo.Empresa;
import com.whatsappcenter.modelo.Rol;
import com.whatsappcenter.modelo.Usuario;

public class UsuarioConverter implements AbstractConverter<Usuario, UsuarioDTO> {


    @Override
    public Usuario fromDto(UsuarioDTO dto) {
        Usuario entity = new Usuario();
        entity.setIdusuario(dto.getIdusuario());
        entity.setIdempresa(new Empresa(dto.getIdempresa()));
        entity.setApellidos(dto.getApellidos());
        entity.setNombres(dto.getNombres());
        entity.setIdrol(new Rol(dto.getIdrol()));
        entity.setUsuario(dto.getUsuario());
        entity.setClave(dto.getClave());
        return entity;
    }

    @Override
    public UsuarioDTO fromEntity(Usuario entity) {
        UsuarioDTO dto = new UsuarioDTO();
        dto.setIdusuario(entity.getIdusuario());
        dto.setIdempresa(entity.getIdempresa().getIdempresa());
        dto.setRazonsocial(entity.getIdempresa().getRazonsocial());
        dto.setApellidos(entity.getApellidos());
        dto.setNombres(entity.getNombres());
        dto.setIdrol(entity.getIdrol().getIdrol());
        dto.setRol(entity.getIdrol().getDescripcion());
        dto.setUsuario(entity.getUsuario());
        dto.setClave(entity.getClave());
        return dto;
    }
}


