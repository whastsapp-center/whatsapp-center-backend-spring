package com.whatsappcenter.covertidor;

import com.whatsappcenter.dto.ContactoDTO;
import com.whatsappcenter.modelo.Contacto;
import com.whatsappcenter.modelo.NumeroTelefono;

import java.text.SimpleDateFormat;

public class ContactoConverter implements AbstractConverter<Contacto, ContactoDTO> {

    SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    @Override
    public Contacto fromDto(ContactoDTO dto) {
        Contacto entity = new Contacto();
        entity.setIdcontacto(dto.getIdcontacto());
        entity.setIdnumero(new NumeroTelefono(dto.getIdnumero()));
        entity.setCodigo(dto.getCodigo());
        entity.setImagen(dto.getImagen());
        entity.setNombre(dto.getNombre());
        return entity;
    }

    @Override
    public ContactoDTO fromEntity(Contacto entity) {
        ContactoDTO dto = new ContactoDTO();
        dto.setIdcontacto(entity.getIdcontacto());
        dto.setCodigo(entity.getCodigo());
        dto.setNombre(entity.getNombre());
        dto.setImagen(entity.getImagen());
        dto.setFconexion(formatoFecha.format(entity.getFconexion()));
        return dto;
    }
}


