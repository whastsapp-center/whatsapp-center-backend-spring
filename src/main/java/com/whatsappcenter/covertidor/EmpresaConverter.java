package com.whatsappcenter.covertidor;

import com.whatsappcenter.dto.EmpresaDTO;
import com.whatsappcenter.modelo.Empresa;

public class EmpresaConverter implements AbstractConverter<Empresa, EmpresaDTO> {

    @Override
    public Empresa fromDto(EmpresaDTO dto) {
        Empresa entity = new Empresa();
        entity.setIdempresa(dto.getIdempresa());
        entity.setRazonsocial(dto.getRazonsocial());
        entity.setRuc(dto.getRuc());

        return entity;
    }

    @Override
    public EmpresaDTO fromEntity(Empresa entity) {
        EmpresaDTO dto = new EmpresaDTO();
        dto.setIdempresa(entity.getIdempresa());
        dto.setRuc(entity.getRuc());
        dto.setRazonsocial(entity.getRazonsocial());

        return dto;
    }
}


