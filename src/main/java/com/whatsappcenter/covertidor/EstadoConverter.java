package com.whatsappcenter.covertidor;

import com.whatsappcenter.dto.EstadoDTO;
import com.whatsappcenter.modelo.Estado;

public class EstadoConverter implements AbstractConverter<Estado, EstadoDTO> {

    @Override
    public Estado fromDto(EstadoDTO dto) {
        Estado entity = new Estado();
        entity.setIdestado(dto.getIdestado());
        entity.setDescripcion(dto.getDescripcion());
        return entity;
    }

    @Override
    public EstadoDTO fromEntity(Estado entity) {
        EstadoDTO dto = new EstadoDTO();
        dto.setIdestado(entity.getIdestado());
        dto.setDescripcion(entity.getDescripcion());
        return dto;
    }
}


