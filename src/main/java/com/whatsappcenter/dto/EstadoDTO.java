package com.whatsappcenter.dto;

import java.io.Serializable;

public class EstadoDTO implements Serializable {

    private Integer idestado;
    private String descripcion;

    public Integer getIdestado() {
        return idestado;
    }

    public void setIdestado(Integer idestado) {
        this.idestado = idestado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
