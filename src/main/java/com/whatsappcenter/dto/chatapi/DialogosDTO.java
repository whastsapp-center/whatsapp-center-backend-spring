package com.whatsappcenter.dto.chatapi;

import java.io.Serializable;

public class DialogosDTO implements Serializable {

    private String id;
    private String name;
    private String image;
    private Long last_time;

    public DialogosDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getLast_time() {
        return last_time;
    }

    public void setLast_time(Long last_time) {
        this.last_time = last_time;
    }
}
