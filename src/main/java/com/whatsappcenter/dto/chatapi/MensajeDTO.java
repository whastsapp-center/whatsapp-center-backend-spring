package com.whatsappcenter.dto.chatapi;

import java.io.Serializable;

public class MensajeDTO implements Serializable {

    private Integer idcontacto;
    private String mensaje;


    public Integer getIdcontacto() {
        return idcontacto;
    }

    public void setIdcontacto(Integer idcontacto) {
        this.idcontacto = idcontacto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }


}
