package com.whatsappcenter.dto;

import java.io.Serializable;

public class RolDTO implements Serializable {

    private Integer idrol;
    private String descripcion;

    public Integer getIdrol() {
        return idrol;
    }

    public void setIdrol(Integer idrol) {
        this.idrol = idrol;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
