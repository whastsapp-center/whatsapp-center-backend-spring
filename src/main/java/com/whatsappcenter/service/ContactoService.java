package com.whatsappcenter.service;

import com.whatsappcenter.modelo.Contacto;

import java.util.List;

public interface ContactoService {
    public List<Contacto> lista();
    public void registrar(Contacto contacto);
    public void actualizar(Contacto contacto);
    public boolean eliminar(Contacto contacto);
    public Contacto buscarPorIdcontacto(Integer idcontacto);
    public Contacto buscarPorCodigo(String codigo);


}
