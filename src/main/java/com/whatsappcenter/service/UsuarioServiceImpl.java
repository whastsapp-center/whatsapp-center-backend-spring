package com.whatsappcenter.service;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserRecord;
import com.whatsappcenter.dao.IUsuario;
import com.whatsappcenter.modelo.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service("usuarioService")
@Scope(value = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private IUsuario iUsuario;

    static PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Override
    public List<Usuario> listaPorIdempresa(Integer idempresa) {
        return iUsuario.findByIdempresa_Idempresa(idempresa);
    }

    @Override
    public List<Usuario> lista() {
        return iUsuario.findAllByOrderByIdempresa();
    }

    @Override
    @Transactional
    public void registrar(Usuario usuario) {
        try {
            usuario.setClave(encriptarBCrypt(usuario.getClave()));
            iUsuario.save(usuario);


            FirebaseAuth auth= FirebaseAuth.getInstance();
            UserRecord.CreateRequest d=new UserRecord.CreateRequest();
            d.setEmail(usuario.getUsuario());
            d.setPassword(usuario.getClave());
            auth.createUser(d);

        } catch (Exception e) {
            e.printStackTrace();
            throw new ExceptionWhatsapp("El nombre de usuario se encuentra duplicado, ingrese otro");
        }
    }

    @Override
    @Transactional
    public void actualizar(Usuario usuario) {
        try {
            FirebaseAuth auth= FirebaseAuth.getInstance();
            UserRecord userRecord= auth.getUserByEmail(usuario.getUsuario());

            UserRecord.UpdateRequest d=new UserRecord.UpdateRequest(userRecord.getUid());
            d.setEmail(usuario.getUsuario());
            d.setPassword(usuario.getClave());
            auth.updateUser(d);

            usuario.setClave(encriptarBCrypt(usuario.getClave()));
            iUsuario.save(usuario);


        } catch (Exception e) {
            e.printStackTrace();
            throw new ExceptionWhatsapp("El nombre de usuario se encuentra duplicado, ingrese otro");
        }

    }

    @Override
    @Transactional
    public boolean eliminar(Usuario usuario) {
        try {
            iUsuario.delete(usuario);

            FirebaseAuth auth= FirebaseAuth.getInstance();
            UserRecord userRecord= auth.getUserByEmail(usuario.getUsuario());
            auth.deleteUser(userRecord.getUid());

        } catch (Exception e) {
            e.printStackTrace();
            throw new ExceptionWhatsapp("El usuario tiene contactos asignados, imposible eliminar");
        }
        return true;
    }

    @Override
    public Usuario buscarPorIdusuario(Integer idusuario) {
        return iUsuario.findByIdusuario(idusuario);
    }

    @Override
    public Usuario buscarPorUsuarioPorClave(String usuario, String clave) {

        try {
            Usuario u= iUsuario.findByUsuario(usuario);

            if(passwordEncoder.matches(clave,u.getClave())){
                return u;
            }else{
                throw new ExceptionWhatsapp("Las credenciales no coinciden");
            }




        }catch (Exception e){
            
            throw new ExceptionWhatsapp("El nombre de usuario no existe");
        }


    }

    @Override
    public Usuario buscarPorUsuario(String usuario) {
        return iUsuario.findByUsuario(usuario);
    }

    public static String encriptarBCrypt(String ccadena) {
        return  passwordEncoder.encode(ccadena);
    }
}
