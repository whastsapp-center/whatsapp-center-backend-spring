package com.whatsappcenter.service;

import com.whatsappcenter.dao.IContacto;
import com.whatsappcenter.modelo.Contacto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("contactoService")
@Scope(value = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ContactoServiceImpl implements ContactoService{

    @Autowired
    private IContacto iContacto;

    @Override
    public List<Contacto> lista() {
        return iContacto.findAll();
    }

    @Override
    @Transactional
    public void registrar(Contacto contacto) {
        try{
            iContacto.save(contacto);
        }catch (Exception e){
            throw new ExceptionWhatsapp(e.getMessage());
        }
    }

    @Override
    @Transactional
    public void actualizar(Contacto contacto) {
        try{
            iContacto.save(contacto);
        }catch (Exception e){
            
            throw new ExceptionWhatsapp(e.getMessage());
        }
    }

    @Override
    @Transactional
    public boolean eliminar(Contacto contacto) {
        try{
            iContacto.delete(contacto);
        }catch (Exception e){
            throw new ExceptionWhatsapp(e.getMessage());
        }

        return true;
    }

    @Override
    public Contacto buscarPorIdcontacto(Integer idcontacto) {
        return iContacto.findByIdcontacto(idcontacto);
    }

    @Override
    public Contacto buscarPorCodigo(String codigo) {
        Contacto contacto=iContacto.findByCodigo(codigo);
        if(contacto==null){
            contacto=new Contacto();
        }
        return contacto;
    }
}
