package com.whatsappcenter.service;

import com.whatsappcenter.dao.IAsignacion;
import com.whatsappcenter.dao.IUsuario;
import com.whatsappcenter.dto.GraficoDTO;
import com.whatsappcenter.modelo.Asignacion;
import com.whatsappcenter.modelo.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service("asignacionService")
@Scope(value = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AsignacionServiceImpl implements AsignacionService {

    @Autowired
    private IAsignacion iAsignacion;
    @Autowired
    private IUsuario iUsuario;

    @Override
    public List<Asignacion> listaPorIdusuario(Integer idusuario) {
        return iAsignacion.findByIdusuario_Idusuario(idusuario);
    }

    @Override
    public List<Asignacion> listaPorIdcontacto(Integer idcontacto) {
        return iAsignacion.findByIdcontacto_Idcontacto(idcontacto);
    }

    @Override
    public List<Asignacion> listaPorIdnumero(Integer idnumero) {
        return iAsignacion.findByIdcontacto_Idnumero_Idnumero(idnumero);
    }

    @Override
    @Transactional
    public void registrar(Asignacion asignacion) {
        try {
            iAsignacion.save(asignacion);
        } catch (Exception e) {
            throw new ExceptionWhatsapp(e.getMessage());
        }
    }

    @Override
    @Transactional
    public void actualizar(Asignacion asignacion) {
        try {
            iAsignacion.save(asignacion);
        } catch (Exception e) {
            throw new ExceptionWhatsapp(e.getMessage());
        }
    }

    @Override
    @Transactional

    public void eliminar(Asignacion asignacion) {
        try {
            iAsignacion.delete(asignacion);
        } catch (Exception e) {
            throw new ExceptionWhatsapp(e.getMessage());
        }
    }

    @Override
    public List<GraficoDTO> listagraficoPorIdempresa(Integer idempresa) {
        List<GraficoDTO> listaRetorno = new ArrayList<>();

        List<Usuario> listaUsuario = iUsuario.findByIdempresa_Idempresa(idempresa);

        List<Asignacion> listaAsignacion;
        GraficoDTO dto;
        for (Usuario u : listaUsuario) {
            dto = new GraficoDTO();
            dto.setIdusuario(u.getIdusuario());
            dto.setApellidos(u.getApellidos());
            dto.setNombres(u.getNombres());
            dto.setUsuario(u.getUsuario());
            listaAsignacion = iAsignacion.findByIdusuario_Idusuario(u.getIdusuario());
            dto.setContactos(listaAsignacion.size());

            listaRetorno.add(dto);
        }

        return listaRetorno;
    }

    @Override
    public List<GraficoDTO> listagraficoPorIdempresaPorIdusuario(Integer idempresa, Integer idusuario) {
        List<GraficoDTO> listaRetorno = new ArrayList<>();

        Usuario u = iUsuario.findByIdusuario(idusuario);

        GraficoDTO dto = new GraficoDTO();
        dto.setIdusuario(u.getIdusuario());
        dto.setApellidos(u.getApellidos());
        dto.setNombres(u.getNombres());
        dto.setUsuario(u.getUsuario());

        List<Asignacion> listaAsignacion = iAsignacion.findByIdusuario_Idusuario(u.getIdusuario());
        dto.setContactos(listaAsignacion.size());

        listaRetorno.add(dto);

        return listaRetorno;
    }
}
