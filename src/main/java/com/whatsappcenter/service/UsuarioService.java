package com.whatsappcenter.service;

import com.whatsappcenter.modelo.Usuario;

import java.util.List;

public interface UsuarioService {
    public List<Usuario> listaPorIdempresa(Integer idempresa);
    public List<Usuario> lista();

    public void registrar(Usuario usuario);
    public void actualizar(Usuario usuario);
    public boolean eliminar(Usuario usuario);
    public Usuario buscarPorIdusuario(Integer idusuario);
    public Usuario buscarPorUsuarioPorClave(String usuario,String clave);
    public Usuario buscarPorUsuario(String usuario);


}
