package com.whatsappcenter.service;

import com.whatsappcenter.dao.IEmpresa;
import com.whatsappcenter.modelo.Empresa;
import com.whatsappcenter.modelo.Estado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service("empresaService")
@Scope(value = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class EmpresaServiceImpl implements EmpresaService{

    @Autowired
    private IEmpresa iEmpresa;

    @Override
    public List<Empresa> lista() {
        return iEmpresa.findAll();
    }

    @Override
    @Transactional
    public void registrar(Empresa empresa) {
        try{
            empresa.setIdestado(Estado.ESTADO_ACTIVO);
            empresa.setFalta(new Date());

            iEmpresa.save(empresa);
        }catch (Exception e){
            
            throw new ExceptionWhatsapp("El número de RUC ya existe, ingrese otro");
        }
    }

    @Override
    @Transactional
    public void actualizar(Empresa empresa) {
        try{
            iEmpresa.save(empresa);
        }catch (Exception e){
            
            throw new ExceptionWhatsapp("El número de RUC ya existe, ingrese otro");
        }
    }
    @Override
    @Transactional
    public boolean eliminar(Empresa empresa) {
        try{
            iEmpresa.delete(empresa);
        }catch (Exception e){
            throw new ExceptionWhatsapp("La empresa tiene usuarios asignados, imposible eliminar");
        }
        return true;
    }
    @Override
    public Empresa buscarPorIdempresa(Integer idempresa) {
        return iEmpresa.findByIdempresa(idempresa);
    }

    @Override
    public Empresa buscarPorRuc(String ruc) {
        return iEmpresa.findByRuc(ruc);
    }
}
