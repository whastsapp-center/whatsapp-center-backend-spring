package com.whatsappcenter.service;

import com.whatsappcenter.modelo.NumeroTelefono;

import java.util.List;

public interface NumeroTelefonoService {

    public List<NumeroTelefono> listaPorIdempresa(Integer idempresa);

    public List<NumeroTelefono> lista();

    public void registrar(NumeroTelefono numeroTelefono);
    public void actualizar(NumeroTelefono numeroTelefono);
    public void eliminar(NumeroTelefono numeroTelefono);
    public NumeroTelefono buscarPorIdnumero(Integer idnumero);
    public NumeroTelefono buscarPorNumero(String numero);


}
