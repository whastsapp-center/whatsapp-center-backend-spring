package com.whatsappcenter.service;

import com.whatsappcenter.modelo.Rol;

import java.util.List;

public interface RolService {

    public List<Rol> lista();
    public Rol buscar(Integer idrol);



}
