package com.whatsappcenter.service;

import com.whatsappcenter.dto.GraficoDTO;
import com.whatsappcenter.modelo.Asignacion;

import java.util.List;

public interface AsignacionService {
    public List<Asignacion> listaPorIdusuario(Integer idusuario);
    public List<Asignacion> listaPorIdcontacto(Integer idcontacto);
    public List<Asignacion> listaPorIdnumero(Integer idnumero);

    public void registrar(Asignacion asignacion);
    public void actualizar(Asignacion asignacion);
    public void eliminar(Asignacion asignacion);

    public List<GraficoDTO> listagraficoPorIdempresa(Integer idempresa);
    public List<GraficoDTO> listagraficoPorIdempresaPorIdusuario(Integer idempresa, Integer idusuario);

}
