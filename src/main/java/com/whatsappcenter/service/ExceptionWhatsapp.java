package com.whatsappcenter.service;

public class ExceptionWhatsapp extends RuntimeException  {

    public ExceptionWhatsapp(String message) {
        super(message);
    }
}
