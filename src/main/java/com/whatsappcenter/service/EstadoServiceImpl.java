package com.whatsappcenter.service;

import com.whatsappcenter.dao.IEstado;
import com.whatsappcenter.modelo.Estado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("estadoService")
@Scope(value = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class EstadoServiceImpl implements EstadoService{

    @Autowired
    private IEstado iEstado;

    @Override
    public List<Estado> lista() {
        return iEstado.findAll();
    }
}
