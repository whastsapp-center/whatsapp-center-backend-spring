package com.whatsappcenter.service;

import com.whatsappcenter.modelo.Empresa;

import java.util.List;

public interface EmpresaService {
    public List<Empresa> lista();
    public void registrar(Empresa empresa);
    public void actualizar(Empresa empresa);
    public boolean eliminar(Empresa empresa);
    public Empresa buscarPorIdempresa(Integer idempresa);
    public Empresa buscarPorRuc(String ruc);


}
