package com.whatsappcenter.service;

import com.whatsappcenter.dao.INumeroTelefono;
import com.whatsappcenter.modelo.NumeroTelefono;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("numeroTelefonoService")
@Scope(value = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class NumeroTelefonoServiceImpl implements NumeroTelefonoService{

    @Autowired
    private INumeroTelefono iNumeroTelefono;


    @Override
    public List<NumeroTelefono> listaPorIdempresa(Integer idempresa) {
        return iNumeroTelefono.findByIdempresa_Idempresa(idempresa);
    }

    @Override
    public List<NumeroTelefono> lista() {
        return iNumeroTelefono.findAll();
    }

    @Override
    @Transactional
    public void registrar(NumeroTelefono numeroTelefono) {
        try{
            iNumeroTelefono.save(numeroTelefono);
        }catch (Exception e){
            
            throw new ExceptionWhatsapp(e.getMessage());
        }
    }

    @Override
    @Transactional
    public void actualizar(NumeroTelefono numeroTelefono) {
        try{
            iNumeroTelefono.save(numeroTelefono);
        }catch (Exception e){
            
            throw new ExceptionWhatsapp(e.getMessage());
        }
    }

    @Override
    @Transactional
    public void eliminar(NumeroTelefono numeroTelefono) {
        try{
            iNumeroTelefono.delete(numeroTelefono);
        }catch (Exception e){
            
            throw new ExceptionWhatsapp(e.getMessage());
        }
    }

    @Override
    public NumeroTelefono buscarPorIdnumero(Integer idnumero) {
        return iNumeroTelefono.findByIdnumero(idnumero);
    }

    @Override
    public NumeroTelefono buscarPorNumero(String numero) {
        return iNumeroTelefono.findByNumero(numero);
    }
}
