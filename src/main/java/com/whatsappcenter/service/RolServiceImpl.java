package com.whatsappcenter.service;

import com.whatsappcenter.dao.IRol;
import com.whatsappcenter.modelo.Rol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("rolService")
@Scope(value = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RolServiceImpl implements RolService{

    @Autowired
    private IRol iRol;

    @Override
    public List<Rol> lista() {
        return iRol.findAll();
    }


    @Override
    public Rol buscar(Integer idrol) {
        return iRol.findByIdrol(idrol);
    }
}
