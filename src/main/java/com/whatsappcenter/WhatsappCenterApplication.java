package com.whatsappcenter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WhatsappCenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(WhatsappCenterApplication.class, args);
	}

}
