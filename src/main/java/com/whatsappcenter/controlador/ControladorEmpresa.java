package com.whatsappcenter.controlador;

import com.google.gson.Gson;
import com.whatsappcenter.covertidor.EmpresaConverter;
import com.whatsappcenter.dto.EmpresaDTO;
import com.whatsappcenter.modelo.Empresa;
import com.whatsappcenter.modelo.Rol;
import com.whatsappcenter.modelo.Usuario;
import com.whatsappcenter.service.EmpresaService;
import com.whatsappcenter.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping
public class ControladorEmpresa {
    @Autowired
    private EmpresaService empresaService;
    @Autowired
    private UsuarioService usuarioService;


    private Gson gson = new Gson();
    private EmpresaConverter converter =new EmpresaConverter();

    @CrossOrigin
    @GetMapping("/empresa/lista/{idusuario}")
    public ResponseEntity<String> lista(@PathVariable int idusuario){
        Usuario usuario=usuarioService.buscarPorIdusuario(idusuario);
        List<Empresa> listaRetorno=new ArrayList<>();

        if(usuario.getIdrol().getIdrol().equals(Rol.ROOT.getIdrol())){
            listaRetorno=empresaService.lista();
        }else{
            listaRetorno.add(usuario.getIdempresa());
        }
        return ResponseEntity.ok(gson.toJson(converter.fromEntity(listaRetorno)));
    }

    @CrossOrigin
    @PostMapping("/empresa/registrar")
    @ResponseBody
    public ResponseEntity<String> registrar(@RequestBody EmpresaDTO empresaDTO){
        Empresa empresa=converter.fromDto(empresaDTO);
        try{
            empresaService.registrar(empresa);
            return ResponseEntity.ok(gson.toJson(converter.fromEntity(empresa)));
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }



    }

    @CrossOrigin
    @PutMapping("/empresa/actualizar")
    @ResponseBody
    public ResponseEntity<String> actualizar(@RequestBody EmpresaDTO empresaDTO){

        Empresa empresa=converter.fromDto(empresaDTO);
        try{
            empresaService.actualizar(empresa);
            return ResponseEntity.ok(gson.toJson(converter.fromEntity(empresa)));
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }


    }

    @CrossOrigin
    @DeleteMapping("/empresa/eliminar/{idempresa}")
    @ResponseBody
    public ResponseEntity<String> eliminar(@PathVariable int idempresa){
        Empresa empresa=empresaService.buscarPorIdempresa(idempresa);

        try{
            empresaService.eliminar(empresa);
            return ResponseEntity.ok(gson.toJson(converter.fromEntity(empresa)));
        }catch (Exception e){
            return ResponseEntity.badRequest().body("Imposible eliminar la empresa tiene usuarios asignados");

        }


    }


}
