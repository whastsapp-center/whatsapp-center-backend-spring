package com.whatsappcenter.controlador;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.whatsappcenter.covertidor.ContactoConverter;
import com.whatsappcenter.dto.ContactoDTO;
import com.whatsappcenter.dto.chatapi.DialogosDTO;
import com.whatsappcenter.modelo.*;
import com.whatsappcenter.service.AsignacionService;
import com.whatsappcenter.service.ContactoService;
import com.whatsappcenter.service.NumeroTelefonoService;
import com.whatsappcenter.service.UsuarioService;
import com.whatsappcenter.util.ApiChatService;
import com.whatsappcenter.util.Fecha;
import com.whatsappcenter.util.HttpCliente;
import com.whatsappcenter.util.OrdenarPorFconexionContactoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping
public class ControladorContacto {
    @Autowired
    private NumeroTelefonoService numeroTelefonoService;
    @Autowired
    private ContactoService contactoService;
    @Autowired
    private AsignacionService asignacionService;
    @Autowired
    private UsuarioService usuarioService;

    private Gson gson = new Gson();
    private ContactoConverter converter =new ContactoConverter();


    @CrossOrigin
    @GetMapping("/contacto/lista/{idnumero}/{idusuario}")
    public ResponseEntity<String> lista(@PathVariable int idnumero,@PathVariable int idusuario){

        Usuario usuario=usuarioService.buscarPorIdusuario(idusuario);
        //EXTRAEMOS TODOS LOS CONTACTOS ASGINADOS
        List<Asignacion> listaAsignacion;

        //SI ES ADMINISTRADOR VERIFICAMOS QUE HAYA NUEVOS CONTACTOS
        if(usuario.getIdrol().getIdrol().equals(Rol.ADMINISTRADOR.getIdrol())){
            NumeroTelefono numeroTelefono=numeroTelefonoService.buscarPorIdnumero(idnumero);
            JsonObject jsonObject=HttpCliente.get(ApiChatService.getDialogs(numeroTelefono.getInstance(),numeroTelefono.getToken()));

            Type listType = new TypeToken<ArrayList<DialogosDTO>>(){}.getType();
            List<DialogosDTO> listaDTO= gson.fromJson(jsonObject.get("dialogs"),listType);

            Contacto contacto;
            List<Asignacion> listaDetalle;
            Asignacion asignacion;
            for(DialogosDTO d:listaDTO){
                contacto=contactoService.buscarPorCodigo(d.getId());

                contacto.setNombre(d.getName());
                contacto.setImagen(d.getImage());
                contacto.setCodigo(d.getId());
                contacto.setIdnumero(numeroTelefono);
                contacto.setFconexion(Fecha.milisegundosToFecha(d.getLast_time()*1000));

                if(d.getLast_time()<=0){
                    contacto.setFconexion(Fecha.primerDiaMes(new Date()));
                }

                if(d.getImage().isEmpty()){
                    contacto.setImagen("https://whatsappcenter.tk:8443/api/img/usuario.png");
                }
                contactoService.registrar(contacto);

                listaDetalle=asignacionService.listaPorIdcontacto(contacto.getIdcontacto());
                if(listaDetalle.isEmpty()){
                    asignacion=new Asignacion();
                    asignacion.setIdcontacto(contacto);
                    asignacion.setFregistro(new Date());
                    asignacion.setIdusuario(usuario);
                    asignacionService.registrar(asignacion);
                }
            }

            listaAsignacion=asignacionService.listaPorIdnumero(idnumero);
        }else{
            listaAsignacion=asignacionService.listaPorIdusuario(idusuario);
        }


        List<ContactoDTO> listaDTOContacto=new ArrayList<>();

        ContactoDTO dto;
        for(Asignacion a:listaAsignacion){
            dto=converter.fromEntity(a.getIdcontacto());
            dto.setIdusuario(a.getIdusuario().getIdusuario());
            dto.setNombreusuario(a.getIdusuario().getNombres()+" - "+a.getIdusuario().getApellidos());
            listaDTOContacto.add(dto);
        }

        Collections.sort(listaDTOContacto,new OrdenarPorFconexionContactoDTO());

        return ResponseEntity.ok(gson.toJson(listaDTOContacto));
    }

    @CrossOrigin
    @PutMapping("/contacto/actualizar")
    @ResponseBody
    public ResponseEntity<String> actualizar(@RequestBody ContactoDTO contactoDTO){
        try{

            List<Asignacion> listaAsignacion=asignacionService.listaPorIdcontacto(contactoDTO.getIdcontacto());
            for(Asignacion asignacion:listaAsignacion){
                asignacion.setIdusuario(new Usuario(contactoDTO.getIdusuario()));
                asignacionService.actualizar(asignacion);
            }

            return ResponseEntity.ok("Actualizacion exitosa");
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }



    }



}
