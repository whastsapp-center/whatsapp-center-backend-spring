package com.whatsappcenter.controlador;

import com.google.gson.Gson;
import com.whatsappcenter.covertidor.RolConverter;
import com.whatsappcenter.modelo.Rol;
import com.whatsappcenter.service.RolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping
public class ControladorRol {
    @Autowired
    private RolService rolService;

    private Gson gson = new Gson();
    private RolConverter converter =new RolConverter();


    @CrossOrigin
    @GetMapping("/rol/lista/{idrol}")
    public ResponseEntity<String> lista(@PathVariable Integer idrol){

        List<Rol> listaRetorno=new ArrayList<>();
        if(idrol.equals(Rol.ADMINISTRADOR.getIdrol())){
            listaRetorno.add(rolService.buscar(Rol.ADMINISTRADOR.getIdrol()));
            listaRetorno.add(rolService.buscar(Rol.AGENTE.getIdrol()));
        }else if(idrol.equals(Rol.ROOT.getIdrol())){
            listaRetorno=rolService.lista();
        }


        return ResponseEntity.ok(gson.toJson(converter.fromEntity(listaRetorno)));
    }

}
