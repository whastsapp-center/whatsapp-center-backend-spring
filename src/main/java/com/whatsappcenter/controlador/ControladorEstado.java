package com.whatsappcenter.controlador;

import com.google.gson.Gson;
import com.whatsappcenter.covertidor.EstadoConverter;
import com.whatsappcenter.modelo.Estado;
import com.whatsappcenter.service.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping
public class ControladorEstado {
    @Autowired
    private EstadoService estadoService;

    private Gson gson = new Gson();
    private EstadoConverter converter =new EstadoConverter();

    @CrossOrigin
    @GetMapping("/estado/lista")
    public ResponseEntity<String> lista(){
        List<Estado> listaEstado=estadoService.lista();
        return ResponseEntity.ok(gson.toJson(converter.fromEntity(listaEstado)));
    }
}
