package com.whatsappcenter.controlador;

import com.google.gson.Gson;
import com.whatsappcenter.covertidor.UsuarioConverter;
import com.whatsappcenter.dto.UsuarioDTO;
import com.whatsappcenter.modelo.Rol;
import com.whatsappcenter.modelo.Usuario;
import com.whatsappcenter.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping
public class ControladorUsuario {
    @Autowired
    private UsuarioService usuarioService;

    private Gson gson = new Gson();
    private UsuarioConverter converter =new UsuarioConverter();


    @CrossOrigin
    @PostMapping("/usuario/login")
    @ResponseBody
    public ResponseEntity<String> login(@RequestBody UsuarioDTO usuarioDTO){

        try{
            Usuario usuario=  usuarioService.buscarPorUsuarioPorClave(usuarioDTO.getUsuario(),usuarioDTO.getClave());
            return ResponseEntity.ok(gson.toJson(converter.fromEntity(usuario)));
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }

    @CrossOrigin
    @GetMapping("/usuario/lista/{idempresa}")
    public ResponseEntity<String> lista(@PathVariable int idempresa){
        List<Usuario> listaUsuario=usuarioService.listaPorIdempresa(idempresa);
        return ResponseEntity.ok(gson.toJson(converter.fromEntity(listaUsuario)));
    }

    @CrossOrigin
    @GetMapping("/usuario/lista2/{idempresa}/{idusuario}")
    public ResponseEntity<String> lista2(@PathVariable int idempresa,@PathVariable int idusuario){
        Usuario usuario=usuarioService.buscarPorIdusuario(idusuario);
        List<Usuario> listaRetorno=new ArrayList<>();
        if(usuario.getIdrol().getIdrol().equals(Rol.ROOT.getIdrol())){
            listaRetorno=usuarioService.lista();
        }else{
            listaRetorno=usuarioService.listaPorIdempresa(idempresa);
        }


        return ResponseEntity.ok(gson.toJson(converter.fromEntity(listaRetorno)));
    }

    @CrossOrigin
    @PostMapping("/usuario/registrar")
    @ResponseBody
    public ResponseEntity<String> registrar(@RequestBody UsuarioDTO usuarioDTO){
        Usuario usuario=converter.fromDto(usuarioDTO);

        try{

            usuarioService.registrar(usuario);
            return ResponseEntity.ok(gson.toJson(converter.fromEntity(usuario)));
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }

    @CrossOrigin
    @PutMapping("/usuario/actualizar")
    @ResponseBody
    public ResponseEntity<String> actualizar(@RequestBody UsuarioDTO usuarioDTO){
        Usuario usuario=converter.fromDto(usuarioDTO);
        try{

            usuarioService.actualizar(usuario);
            return ResponseEntity.ok(gson.toJson(converter.fromEntity(usuario)));
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @CrossOrigin
    @DeleteMapping("/usuario/eliminar/{idusuario}")
    @ResponseBody
    public ResponseEntity<String> eliminar(@PathVariable int idusuario){
        Usuario usuario=usuarioService.buscarPorIdusuario(idusuario);
        try{
            usuarioService.eliminar(usuario);
            return ResponseEntity.ok(gson.toJson(converter.fromEntity(usuario)));
        }catch (Exception e){
            return ResponseEntity.badRequest().body("Imposible eliminar el usuario tiene contactos asignados");

        }

    }


}
