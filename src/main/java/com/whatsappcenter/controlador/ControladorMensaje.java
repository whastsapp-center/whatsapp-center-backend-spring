package com.whatsappcenter.controlador;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.whatsappcenter.dto.chatapi.MensajeDTO;
import com.whatsappcenter.dto.chatapi.MessagesDTO;
import com.whatsappcenter.modelo.Contacto;
import com.whatsappcenter.modelo.NumeroTelefono;
import com.whatsappcenter.service.ContactoService;
import com.whatsappcenter.service.NumeroTelefonoService;
import com.whatsappcenter.util.ApiChatService;
import com.whatsappcenter.util.HttpCliente;
import org.apache.http.entity.StringEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping
public class ControladorMensaje {
    @Autowired
    private NumeroTelefonoService numeroTelefonoService;
    @Autowired
    private ContactoService contactoService;

    private Gson gson = new Gson();

    @CrossOrigin
    @GetMapping("/mensaje/lista/{idcontacto}")
    public ResponseEntity<String> lista(@PathVariable int idcontacto){
        Contacto contacto=contactoService.buscarPorIdcontacto(idcontacto);
        NumeroTelefono numeroTelefono=numeroTelefonoService.buscarPorIdnumero(contacto.getIdnumero().getIdnumero());

        JsonObject jsonObject=HttpCliente.get(ApiChatService.getMessages(numeroTelefono.getInstance(),numeroTelefono.getToken(),contacto.getCodigo()));

        Type listType = new TypeToken<ArrayList<MessagesDTO>>(){}.getType();
        List<MessagesDTO> listaDTO= gson.fromJson(jsonObject.get("messages"),listType);
        return ResponseEntity.ok(gson.toJson(listaDTO));
    }

    @CrossOrigin
    @PostMapping("/mensaje/enviar")
    @ResponseBody
    public ResponseEntity<String> enviar(@RequestBody MensajeDTO mensajeDTO){
        try{
            Contacto contacto=contactoService.buscarPorIdcontacto(mensajeDTO.getIdcontacto());
            NumeroTelefono numeroTelefono=numeroTelefonoService.buscarPorIdnumero(contacto.getIdnumero().getIdnumero());

            HashMap<String, String> parametro=new HashMap<>();
            parametro.put("body",mensajeDTO.getMensaje());
            parametro.put("chatId",contacto.getCodigo());

            StringEntity postingString = new StringEntity(gson.toJson(parametro));

            HttpCliente.post(ApiChatService.sendMessage(numeroTelefono.getInstance(),numeroTelefono.getToken()),postingString);

        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }

        return ResponseEntity.ok("");

    }

    @CrossOrigin
    @DeleteMapping("/mensaje/eliminar/{idcontacto}/{messageId}")
    @ResponseBody
    public ResponseEntity<String> eliminar(@PathVariable int idcontacto,@PathVariable String messageId){
        try{
            Contacto contacto=contactoService.buscarPorIdcontacto(idcontacto);
            NumeroTelefono numeroTelefono=numeroTelefonoService.buscarPorIdnumero(contacto.getIdnumero().getIdnumero());

            HashMap<String, String> parametro=new HashMap<>();
            parametro.put("messageId",messageId);

            StringEntity postingString = new StringEntity(gson.toJson(parametro));

            HttpCliente.post(ApiChatService.deleteMessage(numeroTelefono.getInstance(),numeroTelefono.getToken()),postingString);


        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }

        return ResponseEntity.ok("");

    }

}
