package com.whatsappcenter.controlador;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.whatsappcenter.covertidor.NumeroTelefonoConverter;
import com.whatsappcenter.dto.NumeroTelefonoDTO;
import com.whatsappcenter.modelo.Empresa;
import com.whatsappcenter.modelo.NumeroTelefono;
import com.whatsappcenter.modelo.Rol;
import com.whatsappcenter.modelo.Usuario;
import com.whatsappcenter.service.NumeroTelefonoService;
import com.whatsappcenter.service.UsuarioService;
import com.whatsappcenter.util.ApiChatService;
import com.whatsappcenter.util.HttpCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping
public class ControladorNumeroTelefono {
    @Autowired
    private NumeroTelefonoService numeroTelefonoService;
    @Autowired
    private UsuarioService usuarioService;

    private Gson gson = new Gson();
    private NumeroTelefonoConverter converter =new NumeroTelefonoConverter();

    @CrossOrigin
    @GetMapping("/numero/lista/{idempresa}")
    public ResponseEntity<String> lista(@PathVariable int idempresa){

        List<NumeroTelefono>  listaRetorno=numeroTelefonoService.listaPorIdempresa(idempresa);
        return ResponseEntity.ok(gson.toJson(converter.fromEntity(listaRetorno)));
    }


    @CrossOrigin
    @GetMapping("/numero/lista2/{idempresa}/{idusuario}")
    public ResponseEntity<String> lista2(@PathVariable int idempresa,@PathVariable int idusuario){

        Usuario usuario=usuarioService.buscarPorIdusuario(idusuario);

        List<NumeroTelefono> listaRetorno=new ArrayList<>();

        if(usuario.getIdrol().getIdrol().equals(Rol.ROOT.getIdrol())){
            listaRetorno=numeroTelefonoService.lista();
        }else{
            listaRetorno=numeroTelefonoService.listaPorIdempresa(idempresa);
        }

        return ResponseEntity.ok(gson.toJson(converter.fromEntity(listaRetorno)));
    }

    @CrossOrigin
    @GetMapping("/numero/listaConfiguracion/{idempresa}")
    public ResponseEntity<String> listaConfiguracion(@PathVariable int idempresa){
        List<NumeroTelefono> listaNumeroTelefono=numeroTelefonoService.listaPorIdempresa(idempresa);
        List<NumeroTelefonoDTO> listaDTO=new ArrayList<>();

        NumeroTelefonoDTO dto;
        JsonObject jsonObject;
        for(NumeroTelefono numero:listaNumeroTelefono){
            dto=converter.fromEntity(numero);
            dto.setQrCode("");
            dto.setRazonsocial(numero.getIdempresa().getRazonsocial());

            if(!numero.getToken().isEmpty()){
                jsonObject=HttpCliente.get(ApiChatService.status(numero.getInstance(),numero.getToken()));
                dto.setAccountStatus(jsonObject.get("accountStatus").getAsString());

                if(!dto.getAccountStatus().contains("authenticated")){
                    dto.setQrCode(jsonObject.get("qrCode").getAsString());
                }
            }



            listaDTO.add(dto);
        }

        return ResponseEntity.ok(gson.toJson(listaDTO));
    }

    @CrossOrigin
    @PostMapping("/numero/registrar")
    @ResponseBody
    public ResponseEntity<String> registrar(@RequestBody NumeroTelefonoDTO numeroTelefonoDTO){
        NumeroTelefono numeroTelefono=converter.fromDto(numeroTelefonoDTO);


        try{
            numeroTelefonoService.registrar(numeroTelefono);
            return ResponseEntity.ok(gson.toJson(converter.fromEntity(numeroTelefono)));
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }


    }

    @CrossOrigin
    @PutMapping("/numero/actualizar")
    @ResponseBody
    public ResponseEntity<String> actualizar(@RequestBody NumeroTelefonoDTO numeroTelefonoDTO){
        NumeroTelefono numeroTelefono=converter.fromDto(numeroTelefonoDTO);
        numeroTelefonoService.actualizar(numeroTelefono);
        return ResponseEntity.ok(gson.toJson(converter.fromEntity(numeroTelefono)));
    }

    @CrossOrigin
    @DeleteMapping("/numero/eliminar/{idnumero}")
    @ResponseBody
    public ResponseEntity<String> eliminar(@PathVariable int idnumero){
        NumeroTelefono numeroTelefono=numeroTelefonoService.buscarPorIdnumero(idnumero);

        try{
            numeroTelefonoService.eliminar(numeroTelefono);
            return ResponseEntity.ok(gson.toJson(converter.fromEntity(numeroTelefono)));
        }catch (Exception e){
            return ResponseEntity.badRequest().body("Imposible eliminar el numero tiene contactos asignados");
        }


    }


    @CrossOrigin
    @GetMapping("/numero/logout/{idnumero}")
    public ResponseEntity<String> logout(@PathVariable int idnumero){
        NumeroTelefono numeroTelefono=numeroTelefonoService.buscarPorIdnumero(idnumero);
        HttpCliente.get(ApiChatService.logout(numeroTelefono.getInstance(),numeroTelefono.getToken()));

        return ResponseEntity.ok("logout");
    }

}
