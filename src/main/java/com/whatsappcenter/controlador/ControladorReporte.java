package com.whatsappcenter.controlador;

import com.google.gson.Gson;
import com.whatsappcenter.dto.GraficoDTO;
import com.whatsappcenter.modelo.Rol;
import com.whatsappcenter.modelo.Usuario;
import com.whatsappcenter.service.AsignacionService;
import com.whatsappcenter.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping
public class ControladorReporte {
    @Autowired
    private AsignacionService asignacionService;
    @Autowired
    private UsuarioService usuarioService;

    private Gson gson = new Gson();

    @CrossOrigin
    @GetMapping("/reporte/{idempresa}/{idusuario}")
    public ResponseEntity<String> grafico(@PathVariable int idempresa,@PathVariable int idusuario){

        Usuario usuario=usuarioService.buscarPorIdusuario(idusuario);
        //EXTRAEMOS TODOS LOS CONTACTOS ASGINADOS
        List<GraficoDTO> listaRetorno;

        //SI ES ADMINISTRADOR VERIFICAMOS QUE HAYA NUEVOS CONTACTOS
        if(usuario.getIdrol().getIdrol().equals(Rol.ADMINISTRADOR.getIdrol())){

            listaRetorno=asignacionService.listagraficoPorIdempresa(idempresa);
        }else{
            listaRetorno=asignacionService.listagraficoPorIdempresaPorIdusuario(idempresa,idusuario);
        }

        return ResponseEntity.ok(gson.toJson(listaRetorno));
    }



}
