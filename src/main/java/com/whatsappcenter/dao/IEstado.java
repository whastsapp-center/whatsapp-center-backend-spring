package com.whatsappcenter.dao;

import com.whatsappcenter.modelo.Estado;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IEstado extends JpaRepository<Estado,Integer> {

    List<Estado> findAll();

}
