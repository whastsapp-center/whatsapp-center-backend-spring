package com.whatsappcenter.dao;

import com.whatsappcenter.modelo.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IUsuario extends JpaRepository<Usuario,Integer> {

    List<Usuario> findByIdempresa_Idempresa(Integer idempresa);
    List<Usuario> findAllByOrderByIdempresa();

    Usuario findByIdusuario(Integer idusuario);
    Usuario findByUsuario(String usuario);


}
