package com.whatsappcenter.dao;

import com.whatsappcenter.modelo.NumeroTelefono;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface INumeroTelefono extends JpaRepository<NumeroTelefono,Integer> {

    List<NumeroTelefono> findAll();
    List<NumeroTelefono> findByIdempresa_Idempresa(Integer idempresa);
    NumeroTelefono findByIdnumero(Integer idnumero);
    NumeroTelefono findByNumero(String numero);


}
