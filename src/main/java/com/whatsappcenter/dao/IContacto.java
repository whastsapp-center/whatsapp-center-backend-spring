package com.whatsappcenter.dao;

import com.whatsappcenter.modelo.Contacto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IContacto extends JpaRepository<Contacto,Integer> {

    List<Contacto> findAll();
    Contacto findByIdcontacto(Integer idcontacto);
    Contacto findByCodigo(String codigo);

}
