package com.whatsappcenter.dao;

import com.whatsappcenter.modelo.Rol;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IRol extends JpaRepository<Rol,Integer> {

    Rol findByIdrol(Integer idrol);
    List<Rol> findAll();

}
