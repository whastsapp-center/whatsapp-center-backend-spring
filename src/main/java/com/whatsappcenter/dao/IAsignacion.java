package com.whatsappcenter.dao;

import com.whatsappcenter.modelo.Asignacion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IAsignacion extends JpaRepository<Asignacion,Integer> {

    List<Asignacion> findAll();
    List<Asignacion> findByIdusuario_Idusuario(Integer idusuario);
    List<Asignacion> findByIdcontacto_Idcontacto(Integer idcontacto);
    List<Asignacion> findByIdcontacto_Idnumero_Idnumero(Integer idnumero);



}
