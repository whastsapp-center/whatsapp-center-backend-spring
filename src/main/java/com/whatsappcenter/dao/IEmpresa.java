package com.whatsappcenter.dao;

import com.whatsappcenter.modelo.Empresa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IEmpresa extends JpaRepository<Empresa,Integer> {

    List<Empresa> findAll();
    Empresa findByIdempresa(Integer idempresa);
    Empresa findByRuc(String ruc);


}
