package com.whatsappcenter.modelo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="asignacion")
public class Asignacion implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idasignacion;

    @JoinColumn(name = "idusuario", referencedColumnName = "idusuario")
    @ManyToOne(fetch = FetchType.EAGER)
    private Usuario idusuario;

    @JoinColumn(name = "idcontacto", referencedColumnName = "idcontacto")
    @ManyToOne(fetch = FetchType.EAGER)
    private Contacto idcontacto;

    @Column(name = "fregistro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fregistro;

    public Asignacion() {
    }

    public Integer getIdasignacion() {
        return idasignacion;
    }

    public void setIdasignacion(Integer idasignacion) {
        this.idasignacion = idasignacion;
    }

    public Usuario getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Usuario idusuario) {
        this.idusuario = idusuario;
    }

    public Contacto getIdcontacto() {
        return idcontacto;
    }

    public void setIdcontacto(Contacto idcontacto) {
        this.idcontacto = idcontacto;
    }

    public Date getFregistro() {
        return fregistro;
    }

    public void setFregistro(Date fregistro) {
        this.fregistro = fregistro;
    }
}
