package com.whatsappcenter.modelo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name="empresa")
public class Empresa implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idempresa;

    @Column(name = "razonsocial")
    private String razonsocial;

    @Column(name = "ruc")
    private String ruc;

    @Column(name = "falta")
    @Temporal(TemporalType.DATE)
    private Date falta;

    @Column(name = "fbaja")
    @Temporal(TemporalType.DATE)
    private Date fbaja;

    @Column(name = "fregistro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fregistro;

    @JoinColumn(name = "idestado", referencedColumnName = "idestado")
    @ManyToOne(fetch = FetchType.EAGER)
    private Estado idestado;


    public Empresa() {
        this.fregistro=new Date();
    }

    public Empresa(Integer idempresa) {
        this.idempresa = idempresa;
        this.fregistro=new Date();
    }

    public Integer getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(Integer idempresa) {
        this.idempresa = idempresa;
    }

    public String getRazonsocial() {
        return razonsocial;
    }

    public void setRazonsocial(String razonsocial) {
        this.razonsocial = razonsocial;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public Date getFalta() {
        return falta;
    }

    public void setFalta(Date falta) {
        this.falta = falta;
    }

    public Date getFbaja() {
        return fbaja;
    }

    public void setFbaja(Date fbaja) {
        this.fbaja = fbaja;
    }

    public Date getFregistro() {
        return fregistro;
    }

    public void setFregistro(Date fregistro) {
        this.fregistro = fregistro;
    }

    public Estado getIdestado() {
        return idestado;
    }

    public void setIdestado(Estado idestado) {
        this.idestado = idestado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Empresa)) return false;
        Empresa empresa = (Empresa) o;
        return idempresa.equals(empresa.idempresa);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idempresa);
    }
}
