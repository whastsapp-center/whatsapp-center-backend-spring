package com.whatsappcenter.modelo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="rol")
public class Rol implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idrol;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fregistro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fregistro;

    public static final Rol ADMINISTRADOR=new Rol(1);
    public static final Rol AGENTE=new Rol(2);
    public static final Rol ROOT=new Rol(3);




    public Rol() {
    }

    public Rol(Integer idrol) {
        this.idrol = idrol;
    }

    public Integer getIdrol() {
        return idrol;
    }

    public void setIdrol(Integer idrol) {
        this.idrol = idrol;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFregistro() {
        return fregistro;
    }

    public void setFregistro(Date fregistro) {
        this.fregistro = fregistro;
    }
}
