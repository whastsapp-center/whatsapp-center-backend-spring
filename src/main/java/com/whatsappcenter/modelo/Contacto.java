package com.whatsappcenter.modelo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name="contacto")
public class Contacto implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idcontacto;

    @JoinColumn(name = "idnumero", referencedColumnName = "idnumero")
    @ManyToOne(fetch = FetchType.EAGER)
    private NumeroTelefono idnumero;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "imagen")
    private String imagen;

    @Column(name = "fconexion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fconexion;

    @Column(name = "fregistro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fregistro;

    @Column(name = "codigo")
    private String codigo;


    public Contacto() {
        fregistro=new Date();
    }

    public Contacto(Integer idcontacto) {
        this.idcontacto = idcontacto;
    }

    public Integer getIdcontacto() {
        return idcontacto;
    }

    public void setIdcontacto(Integer idcontacto) {
        this.idcontacto = idcontacto;
    }

    public NumeroTelefono getIdnumero() {
        return idnumero;
    }

    public void setIdnumero(NumeroTelefono idnumero) {
        this.idnumero = idnumero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFregistro() {
        return fregistro;
    }

    public void setFregistro(Date fregistro) {
        this.fregistro = fregistro;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Date getFconexion() {
        return fconexion;
    }

    public void setFconexion(Date fconexion) {
        this.fconexion = fconexion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Contacto)) return false;
        Contacto contacto = (Contacto) o;
        return idcontacto.equals(contacto.idcontacto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idcontacto);
    }
}
