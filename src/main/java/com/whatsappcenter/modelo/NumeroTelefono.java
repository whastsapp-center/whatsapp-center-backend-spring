package com.whatsappcenter.modelo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name="numero_telefono")
public class NumeroTelefono implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idnumero;

    @JoinColumn(name = "idempresa", referencedColumnName = "idempresa")
    @ManyToOne(fetch = FetchType.EAGER)
    private Empresa idempresa;

    @Column(name = "numero")
    private String numero;


    @Column(name = "instance")
    private String instance;

    @Column(name = "token")
    private String token;

    @Column(name = "fregistro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fregistro;

    public NumeroTelefono() {
        this.fregistro=new Date();
    }

    public NumeroTelefono(Integer idnumero) {
        this.fregistro=new Date();
        this.idnumero = idnumero;
    }

    public Integer getIdnumero() {
        return idnumero;
    }

    public void setIdnumero(Integer idnumero) {
        this.idnumero = idnumero;
    }

    public Empresa getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(Empresa idempresa) {
        this.idempresa = idempresa;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Date getFregistro() {
        return fregistro;
    }

    public void setFregistro(Date fregistro) {
        this.fregistro = fregistro;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NumeroTelefono)) return false;
        NumeroTelefono that = (NumeroTelefono) o;
        return idnumero.equals(that.idnumero);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idnumero);
    }
}
