package com.whatsappcenter.util;

import com.whatsappcenter.dto.ContactoDTO;
import java.util.Comparator;

/**
 *
 * @author saisa
 */
public class OrdenarPorFconexionContactoDTO implements Comparator{


    @Override
    public int compare(Object o1, Object o2) {
        ContactoDTO c1 = (ContactoDTO) o1;
        ContactoDTO c2 = (ContactoDTO) o2;

        return c2.getFconexion().compareTo(c1.getFconexion());
    }
}
