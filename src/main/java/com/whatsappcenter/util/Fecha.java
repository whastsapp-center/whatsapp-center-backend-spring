package com.whatsappcenter.util;

import java.util.Calendar;
import java.util.Date;

public class Fecha {

    public static Date milisegundosToFecha(Long milisegundos) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(milisegundos);
        return cal.getTime();
    }

    public static Date primerDiaMes(Date dfecha) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dfecha);
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.getActualMinimum(Calendar.DAY_OF_MONTH), cal.getMinimum(Calendar.HOUR_OF_DAY), cal.getMinimum(Calendar.MINUTE), cal.getMinimum(Calendar.SECOND));
        return cal.getTime();
    }

}
