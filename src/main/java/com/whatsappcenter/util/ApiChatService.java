package com.whatsappcenter.util;

public class ApiChatService {

    private static String path ="https://api.chat-api.com/instance";

    public static String getDialogs(String instance,String token){
        return path +instance+"/dialogs?token="+token;
    }

    public static String getMessages(String instance,String token,String chatid){
        return path +instance+"/messages?token="+token+"&chatId="+chatid ;
    }

    public static String sendMessage(String instance,String token){
        return path +instance+"/sendMessage?token="+token;
    }

    public static String deleteMessage(String instance,String token){
        return path +instance+"/deleteMessage?token="+token;
    }

    public static String status(String instance,String token){
        return path +instance+"/status?token="+token;
    }

    public static String logout(String instance,String token){
        return path +instance+"/logout?token="+token;
    }



}
