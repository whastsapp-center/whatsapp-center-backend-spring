package com.whatsappcenter.util;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.FileInputStream;

@Service
public class FBInitialize {

    @PostConstruct
    public void initialize() {
        try {

            ClassLoader classLoader=Thread.currentThread().getContextClassLoader();
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(classLoader.getResourceAsStream("whatsapp-center-a1107-firebase-adminsdk-t7sse-a6974cdf45.json")))
                    .setDatabaseUrl("https://whatsapp-center-a1107-default-rtdb.firebaseio.com/")
                    .build();

            FirebaseApp.initializeApp(options);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
