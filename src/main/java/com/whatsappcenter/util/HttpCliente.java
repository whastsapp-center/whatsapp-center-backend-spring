package com.whatsappcenter.util;

/**
 * @author saisa
 */

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.whatsappcenter.service.ExceptionWhatsapp;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

public class HttpCliente {


    public static JsonObject get(String url) {

        String result = "";
        try {


            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpGet getRequest = new HttpGet(url);
            getRequest.addHeader("Content-Type", "application/json");

            // Tratar respuesta del servidor
            HttpResponse response = httpClient.execute(getRequest);

            int responseCode = response.getStatusLine().getStatusCode();

            HttpEntity entity = response.getEntity();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = entity.getContent();
                result = convertStreamToString(inputStream);

                inputStream.close();
            }

            JsonObject jsonObject = new JsonParser().parse(result).getAsJsonObject();
            return jsonObject;

        } catch (Exception ex) {
            throw new ExceptionWhatsapp(ex.getMessage());
        }

    }




    public static JsonObject post(String url,StringEntity postingString) {

        String result = "";
        try {
            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost postRequest = new HttpPost(url);
            postRequest.addHeader("Content-Type", "application/json");
            postRequest.setEntity(postingString);

            // Tratar respuesta del servidor
            HttpResponse response = httpClient.execute(postRequest);

            int responseCode = response.getStatusLine().getStatusCode();

            HttpEntity entity = response.getEntity();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = entity.getContent();
                result = convertStreamToString(inputStream);

                inputStream.close();
            }

            JsonObject jsonObject = new JsonParser().parse(result).getAsJsonObject();
            return jsonObject;

        } catch (Exception ex) {
            throw new ExceptionWhatsapp(ex.getMessage());
        }

    }



    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                
            }
        }
        return sb.toString();
    }


}
