package com.whatsappcenter.service;

import com.whatsappcenter.modelo.Empresa;
import com.whatsappcenter.modelo.Rol;
import com.whatsappcenter.modelo.Usuario;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author saisa
 */
@SpringBootTest
public class UsuarioServiceImplTest {
  /*
    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private EmpresaService empresaService;


    @Test
    public void testLista() {
        System.out.println("lista");
        List<Usuario> result = usuarioService.listaPorIdempresa(1);
        assertFalse(result.isEmpty());
    }

    @Test
    public void testBuscarPorIdempresa() {
        System.out.println("buscarPorIdempresa");
        Usuario usuario = new Usuario(1);
        Usuario result = usuarioService.buscarPorIdusuario(1);
        assertEquals(usuario, result);
    }

    @Test
    public void testRegistrar() {
        System.out.println("registrar");
        Empresa empresa = empresaService.buscarPorIdempresa(1);
        Usuario usuario = new Usuario();
        usuario.setIdempresa(empresa);
        usuario.setNombres("Prueba");
        usuario.setApellidos("Prueba");
        usuario.setUsuario("A099");
        usuario.setClave("123456");
        usuario.setIdrol(Rol.ADMINISTRADOR);
        usuario.setFregistro(new Date());

        usuarioService.registrar(usuario);
        assertNotNull(usuario.getIdusuario());
    }

    @Test()
    public void testLoginFallido() {
        System.out.println("login fallido");
        Assertions.assertThrows(ExceptionWhatsapp.class, () -> {
            usuarioService.buscarPorUsuarioPorClave("A099", "123456788");
        });
    }

    @Test
    public void testEliminar() {
        System.out.println("eliminar");
        Usuario usuario = usuarioService.buscarPorUsuario("A099");
        assertTrue(usuarioService.eliminar(usuario));
    }
*/

}
