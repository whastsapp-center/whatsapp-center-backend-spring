package com.whatsappcenter.service;

import com.whatsappcenter.modelo.Contacto;
import com.whatsappcenter.modelo.Empresa;
import com.whatsappcenter.modelo.NumeroTelefono;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 *
 * @author saisa
 */
@SpringBootTest
public class ContactoServiceImplTest {
    /*
    @Autowired
    private ContactoService contactoService;
    @Autowired
    private NumeroTelefonoService numeroTelefonoService;


    @Test
    public void testLista() {
        System.out.println("lista");
        List<Contacto> result = contactoService.lista();
        assertFalse(result.isEmpty());
    }

    @Test
    public void testBuscarPorIdcontacto() {
        System.out.println("buscarPorIdcontacto");
        Contacto contacto=new Contacto(1);
        Contacto result = contactoService.buscarPorIdcontacto(1);
        assertEquals(contacto,result);
    }

    @Test
    public void testBuscarPorCodigo() {
        System.out.println("buscarPorCodigo");
        Contacto contacto=contactoService.buscarPorCodigo("999999");
        assertNull(contacto.getIdcontacto());
    }

    @Test
    public void test1Registrar() {
        System.out.println("registrar");
        NumeroTelefono numeroTelefono=numeroTelefonoService.buscarPorIdnumero(1);
        Contacto contacto = new Contacto();
        contacto.setIdnumero(numeroTelefono);
        contacto.setImagen("");
        contacto.setFconexion(new Date());
        contacto.setCodigo("0001");
        contacto.setNombre("Prueba");
        contacto.setFregistro(new Date());

        contactoService.registrar(contacto);
        assertNotNull(contacto.getIdcontacto());
    }

    @Test
    public void test2Actualizar() {
        System.out.println("actualizar");
        Contacto contacto=contactoService.buscarPorCodigo("0001");
        contacto.setFregistro(new Date());
        contacto.setNombre("Carlos");
        contactoService.actualizar(contacto);

        Contacto contactoVerificar=contactoService.buscarPorCodigo("0001");

        assertEquals(contactoVerificar.getNombre(), "Carlos");

    }

    @Test
    public void test3Eliminar() {
        System.out.println("eliminar");
        Contacto contacto=contactoService.buscarPorCodigo("0001");
        assertTrue(contactoService.eliminar(contacto));
    }

    @Test
    public void test3ErrorEliminar() {
        System.out.println("eliminar");
        Contacto contacto=new Contacto();
        contacto.setIdcontacto(100);


        assertTrue(contactoService.eliminar(contacto));


    }

     */



}
