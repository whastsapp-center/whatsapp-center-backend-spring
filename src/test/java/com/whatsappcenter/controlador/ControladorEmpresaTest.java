package com.whatsappcenter.controlador;

import com.whatsappcenter.covertidor.EmpresaConverter;
import com.whatsappcenter.dto.EmpresaDTO;
import com.whatsappcenter.modelo.Empresa;
import com.whatsappcenter.service.EmpresaService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


@SpringBootTest
public class ControladorEmpresaTest {
   /*
    @Autowired
    private ControladorEmpresa controladorEmpresa;
    @Autowired
    private EmpresaService empresaService;

    private EmpresaConverter converter =new EmpresaConverter();


    @Test
    public void testLista() {
        ResponseEntity<String> httpResponse = controladorEmpresa.lista(1);
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }
    @Test
    public void test1Registrar() {
        EmpresaDTO empresaDTO=new EmpresaDTO();
        empresaDTO.setRuc("10439418350");
        empresaDTO.setRazonsocial("Prueba");

        ResponseEntity<String> httpResponse = controladorEmpresa.registrar(empresaDTO);
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }
    @Test
    public void test1RegistrarDuplicado() {
        EmpresaDTO empresaDTO=new EmpresaDTO();
        empresaDTO.setRuc("10439418350");
        empresaDTO.setRazonsocial("Prueba");

        ResponseEntity<String> httpResponse = controladorEmpresa.registrar(empresaDTO);
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.BAD_REQUEST);
    }
    @Test
    public void test2Actualizar() {
        Empresa empresa=empresaService.buscarPorRuc("10439418350");
        EmpresaDTO empresaDTO=converter.fromEntity(empresa);
        empresaDTO.setRazonsocial("TECSUP");

        ResponseEntity<String> httpResponse = controladorEmpresa.actualizar(empresaDTO);
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }
    @Test
    public void test3Eliminar() {
        Empresa empresa=empresaService.buscarPorRuc("10439418350");
        ResponseEntity<String> httpResponse = controladorEmpresa.eliminar(empresa.getIdempresa());
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }

     */
}
