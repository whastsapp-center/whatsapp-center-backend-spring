package com.whatsappcenter.controlador;

import com.whatsappcenter.covertidor.UsuarioConverter;
import com.whatsappcenter.dto.UsuarioDTO;
import com.whatsappcenter.modelo.Rol;
import com.whatsappcenter.modelo.Usuario;
import com.whatsappcenter.service.EmpresaService;
import com.whatsappcenter.service.UsuarioService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


@SpringBootTest
public class ControladorUsuarioTest {
        /*
    @Autowired
    private ControladorUsuario controladorUsuario;
    @Autowired
    private UsuarioService usuarioService;

    private UsuarioConverter converter =new UsuarioConverter();


    @Test
    public void test1Registrar() {

        UsuarioDTO usuarioDTO=new UsuarioDTO();
        usuarioDTO.setNombres("JORDY");
        usuarioDTO.setApellidos("CARITA");
        usuarioDTO.setIdrol(1);
        usuarioDTO.setIdempresa(1);
        usuarioDTO.setUsuario("A099");
        usuarioDTO.setClave("1234");
        ResponseEntity<String> httpResponse = controladorUsuario.registrar(usuarioDTO);
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }
    @Test
    public void test1RegistrarDuplicado() {
        UsuarioDTO usuarioDTO=new UsuarioDTO();
        usuarioDTO.setNombres("JORDY");
        usuarioDTO.setApellidos("CARITA");
        usuarioDTO.setIdrol(1);
        usuarioDTO.setIdempresa(1);
        usuarioDTO.setUsuario("A099");
        usuarioDTO.setClave("1234");

        ResponseEntity<String> httpResponse = controladorUsuario.registrar(usuarioDTO);
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test
    public void test2Actualizar() {

        Usuario usuario=usuarioService.buscarPorUsuario("A099");

        UsuarioDTO usuarioDTO=converter.fromEntity(usuario);
        usuarioDTO.setClave("1234");

        ResponseEntity<String> httpResponse = controladorUsuario.actualizar(usuarioDTO);
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void test3ActualizarDuplicado() {
        Usuario usuario=usuarioService.buscarPorUsuario("A099");

        UsuarioDTO usuarioDTO=converter.fromEntity(usuario);
        usuarioDTO.setUsuario("demo");

        ResponseEntity<String> httpResponse = controladorUsuario.actualizar(usuarioDTO);
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.BAD_REQUEST);
    }
    @Test
    public void test5Eliminar() {
        Usuario usuario=usuarioService.buscarPorUsuario("A099");
        ResponseEntity<String> httpResponse = controladorUsuario.eliminar(usuario.getIdusuario());
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }
    @Test
    public void testLogin() {
        System.out.println("login");
        UsuarioDTO usuarioDTO=new UsuarioDTO();
        usuarioDTO.setUsuario("A001");
        usuarioDTO.setClave("1234");
        ResponseEntity<String> httpResponse = controladorUsuario.login(usuarioDTO);
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }
    @Test
    public void testLoginFallido() {
        System.out.println("login");
        UsuarioDTO usuarioDTO=new UsuarioDTO();
        usuarioDTO.setUsuario("A001");
        usuarioDTO.setClave("1234ABCD");
        ResponseEntity<String> httpResponse = controladorUsuario.login(usuarioDTO);
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.BAD_REQUEST);
    }
    @Test
    public void testLista() {
        ResponseEntity<String> httpResponse = controladorUsuario.lista(1);
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }

     */
}
