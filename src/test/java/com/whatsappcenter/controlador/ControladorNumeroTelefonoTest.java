package com.whatsappcenter.controlador;

import com.whatsappcenter.covertidor.NumeroTelefonoConverter;
import com.whatsappcenter.dto.NumeroTelefonoDTO;
import com.whatsappcenter.dto.UsuarioDTO;
import com.whatsappcenter.modelo.Empresa;
import com.whatsappcenter.modelo.NumeroTelefono;
import com.whatsappcenter.service.NumeroTelefonoService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


@SpringBootTest
public class ControladorNumeroTelefonoTest {
    /*
    @Autowired
    private ControladorNumeroTelefono controladorNumeroTelefono;
    @Autowired
    private NumeroTelefonoService numeroTelefonoService;

    private NumeroTelefonoConverter converter =new NumeroTelefonoConverter();


    @Test
    public void test1Registrar() {
        NumeroTelefonoDTO numeroTelefonoDTO=new NumeroTelefonoDTO();
        numeroTelefonoDTO.setIdempresa(1);
        numeroTelefonoDTO.setNumero("51999999999");
        numeroTelefonoDTO.setInstance("");
        numeroTelefonoDTO.setToken("");
        ResponseEntity<String> httpResponse = controladorNumeroTelefono.registrar(numeroTelefonoDTO);
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }
    @Test
    public void test1RegistrarDuplicado() {
        NumeroTelefonoDTO numeroTelefonoDTO=new NumeroTelefonoDTO();
        numeroTelefonoDTO.setIdempresa(1);
        numeroTelefonoDTO.setNumero("51999999999");
        numeroTelefonoDTO.setInstance("");
        numeroTelefonoDTO.setToken("");
        ResponseEntity<String> httpResponse = controladorNumeroTelefono.registrar(numeroTelefonoDTO);
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.BAD_REQUEST);
    }
    @Test
    public void test2Actualizar() {
        NumeroTelefono numeroTelefono=numeroTelefonoService.buscarPorNumero("51999999999");

        NumeroTelefonoDTO numeroTelefonoDTO=converter.fromEntity(numeroTelefono);
        numeroTelefonoDTO.setInstance("I001");

        ResponseEntity<String> httpResponse = controladorNumeroTelefono.actualizar(numeroTelefonoDTO);
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }
    @Test
    public void test3Eliminar() {
        NumeroTelefono numeroTelefono=numeroTelefonoService.buscarPorNumero("51999999999");
        ResponseEntity<String> httpResponse = controladorNumeroTelefono.eliminar(numeroTelefono.getIdnumero());
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }
    @Test
    public void testLista() {
        ResponseEntity<String> httpResponse = controladorNumeroTelefono.lista(1);
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }
    @Test
    public void testListaConfiguracion() {
        ResponseEntity<String> httpResponse = controladorNumeroTelefono.listaConfiguracion(1);
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }

     */


    /*
    @Test
    public void testLogout() {
        ResponseEntity<String> httpResponse = controladorNumeroTelefono.logout(1);
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
    }
     */
}
